import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/sass/_variables.scss',
                'resources/sass/app.scss',
                'resources/js/script.js',
                'resources/css/style.css',
                'resources/css/home.css',
                'resources/css/contact.css',
                'resources/css/auth.css',
            ],
            refresh: true,
        }),
    ],
});
