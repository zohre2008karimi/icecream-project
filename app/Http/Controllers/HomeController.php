<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Cryptommer\Smsir\Smsir;

use Carbon\Carbon;

use Symfony\Component\HttpFoundation\Session\Storage;

class HomeController extends Controller
{




    public function __construct()
    {
        $this->middleware('auth');
    }





    public function index()

    {

        $data= DB :: table('users')->pluck('name','email');

        $path = fopen(storage_path('app/Register_User.doc') , "w+") or  die("Error");

        $count =0;

        foreach ($data as  $name => $email) {

           $count++;

           fwrite($path , $count."-".$email." : " .$name . "\n \n");



        }

        fclose($path);


        // send message

       $carbon = new Carbon('-30 second');

       if(DB :: table('users')->latest()->pluck('created_at')->first() >= $carbon )
        {

             $number = DB :: table('users')->latest()->pluck('number')->first() ;
             $name =DB :: table('users')->latest()->pluck('name')->first() ;
             $message ="Dear $name....!". "\n \n". "you registered successfully!". "\n \n"."chocklet-icecream.ir" ;

             $send = smsir::Send();
             $send->bulk($message, [ str($number) ] , null , "30007732006303" );

            return view('home');

        }

        else{

             return view('home');

        }





    }

}
