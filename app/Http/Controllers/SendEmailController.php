<?php

namespace App\Http\Controllers;

use App\Mail\forget_pass_mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


class SendEmailController extends Controller
{
  public function Send_Email( Request $data)
  {

     Mail :: to($data->email)->send( new forget_pass_mail) ;

  }
}
