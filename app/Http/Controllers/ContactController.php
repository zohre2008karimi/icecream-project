<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Validator;



class ContactController extends Controller
{
    public function ContactUs(){

        return view('contact');
    }


    public function  ContactPost( Request  $data)
    {
        //    return $data->all();


        $validatedData = $data->validate([
            'fullname' => 'required',
            'email' => 'required | email | unique:contact_',
            'subject' =>'required | max:60' ,
        ]);


        if( $validatedData){

            DB::table('contact_')->insert([
                'fullname'=>$data->fullname ,
                'email'=>$data->email ,
                'subject'=>$data->subject ,
                'created_at'=>now(),
                'updated_at'=> now() ,
            ]);

            return redirect()->back()->with('success' , 'we recieved your message successfully!') ;

        } else{

            return redirect()->back()->withErrors();
        }

  }


}
