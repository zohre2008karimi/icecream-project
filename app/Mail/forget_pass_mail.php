<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class forget_pass_mail extends Mailable
{
    use Queueable, SerializesModels;



   function __construct()
    {

    }





    public function envelope()
    {
        return new Envelope(
            subject: 'Forget Pass Mail',
        );
    }




    public function content()
    {
        return new Content(
            view: 'forgetpass',
        );
    }




    public function attachments()
    {
        return [];
    }
}
