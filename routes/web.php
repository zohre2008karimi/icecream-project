<?php

use Illuminate\Support\Facades\Route;


use App\Http\Controllers\LogoutController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\HomeController;



Route::get('/', function () {

    return view('content');

})->name('wellcome');


Route::get('/logout', [App\Http\Controllers\LogoutController :: class, 'logout'])->name('logout');
Route :: get('contact-us' ,[ContactController :: class , 'ContactUs'])->name('page.contact');
Route :: post('contact-us' ,[ContactController :: class , 'ContactPost'])->name('page.contactpost');


 Route :: get('about-us' ,[AboutController :: class , 'AboutUs'])->name('page.about');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::post('/validate_login', [App\Http\Controllers\Auth\LoginController::class, 'validate_login'])->name('validate_login');



