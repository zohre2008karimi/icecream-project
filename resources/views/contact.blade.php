@extends('layouts.master')

@section('content')

<div class="row">

    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">

        <form  class="container form-contact" method="POST"  action="{{ route('page.contactpost') }}">
            @csrf

          <label for="fullname">Full Name</label>
          <input type="text" id="fullname" name="fullname" placeholder="Your Fullname.." class="form-control">

          <label for="email">Email</label>
          <input type="text" id="email" name="email" placeholder="Your email.." class="form-control">

          <label for="subject">Message</label>
          <textarea id="subject" name="subject" placeholder="Write something.." style="height:200px" class="form-control"></textarea>
          <br>
          <input type="submit" value="Submit" class="btn btn-success">

        </form>

       @if (Session :: has('success'))
            <div class="alert alert-success alert-success-contact">
                    <p> {{  Session :: get('success') }}</p>
            </div>

        @elseif($errors->any())

                <div class="alert alert-danger">
                    <ul>
                       @foreach($errors->all() as $error)
                           <li>
                              {{ $error }}
                           </li>
                         @endforeach
                      </ul>
                </div>

       @endif

        <img src="{{ asset('images/contact/contact-2.jpg') }}"  id="img-contact">
      </div>

</div>


  @endsection
