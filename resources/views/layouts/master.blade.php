<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0  display:flex " >
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ICeCream</title>
    <link rel="icon"   href="{{ asset('logoes/ice-icon5.png') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    {{-- Bootstrap Links --}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous"></script>

   {{-- icoFonts link --}}
   <link rel="stylesheet" href="{{ asset('icofont/icofont/icofont.min.css') }}">
   {{-- Css file --}}
   {{-- <link rel="stylesheet" href="{{ asset('css/style.css') }}"  type="text/css"> --}}


   {{-- js codes --}}
   <script   src="{{ asset('js/script.js') }}"></script>

   @vite(['resources/css/style.css'  , 'resources/css/auth.css' , 'resources/css/home.css' ,'resources/js/script.js' , 'resources/css/contact.css'])



</head>

<body>

 <div class="container-fluid">
     <div class="row  sticky-top " id="header-1" >
            <div class="col-12">

                <img src="{{ asset('logoes/logo1.png') }}"  id="logo1" class="logo">
                &nbsp;&nbsp;&nbsp;
                <img src="{{ asset('logoes/logo2.png') }}"  id="logo2" class="logo">
                &nbsp;&nbsp;&nbsp;
               <img src="{{ asset('logoes/logo3.png') }}"  id="logo3" class="logo">
               &nbsp;&nbsp;&nbsp;
               <img src="{{ asset('logoes/logo4.png') }}"  id="logo4" class="logo">
               &nbsp;&nbsp;&nbsp;
               <img src="{{ asset('logoes/logo5.png') }}" id="logo5" class="logo">
               &nbsp;&nbsp;&nbsp;
               <img src="{{ asset('logoes/logo6.png') }}" id="logo6"  class="logo">

            </div>
       </div>

     <div class="row fixed-top" id="header-2" >

           <div class="col-12">
                <a href="{{ route('wellcome') }}">
                    <i class="fa fa-home fa-lg">
                    </i>
                </a>


                <a href="{{ route('register') }}" id="p0">
                    <p  class="para">Sign Up</p>
                </a>

                 <a href="{{ route('login') }}" id="p1">
                    <p  class="para">Login</p>
                </a>

                  <a href="{{ route('page.about') }}"  id="p2">
                     <p  class="para">About Us</p>
                  </a>

                 <a href="{{ route ('page.contact')}}"  id="p3"  >
                    <p class="para">Contact Us</p>
                </a>

                <img src="{{ asset('logoes/logo-big.png') }}" id="logo-big">
                <input type="text" placeholder="search" id="input1">
                <i class="icofont-search-2"></i>
           </div>
     </div>



        @yield('content')


      <div class="row container-fluid" id="footer">

         <div class="col-3" id="col-3-1">
                <img src="{{ asset('logoes/logo-big.png') }}" id="logo-footer" >
          </div>

            <div class="col-6">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                <p id="p-foot">2021 Dreyer’s Grand Ice Cream, Inc. and its licensors. All rights reserved</p>
            </div>


            <div class="col-3" id="SocialMedia">
                <i class="icofont-twitter ico-fonts"></i>
                &nbsp;
                <i class="icofont-telegram ico-fonts"></i>
                &nbsp;
                <i class="icofont-instagram ico-fonts"></i>
                &nbsp;
               <i class="icofont-facebook ico-fonts"></i>
            </div>
     </div>
  </div>


</body>
</html>
