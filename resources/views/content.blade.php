@extends('layouts.master')

@section('content')

        <div class="row" id="content">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <h1>The most delicious place on <br> the Internet!</h1>
                <h4>There's so much to explore,here at the home <br>of ice cream.</h4>
                <img src="{{ asset('images/ice.jpeg') }}" >
          </div>
        </div>


       <div class="row"  id="background-1">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <h2 >Oh my, secret recipes!</h2>
                <br>
                <br>


                <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="false">

                    <div class="carousel-indicators">
                      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active " aria-current="true" aria-label="Slide 1" style="background-color: rebeccapurple;"></button>
                      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"  aria-label="Slide 2"  style="background-color: rebeccapurple;"></button>
                      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"  aria-label="Slide 3" style="background-color: rebeccapurple;"></button>
                    </div>


                 {{-- slide.1 --}}
                 <div class="carousel-inner  ">
                     <div class="carousel-item active ">

                        <div class="row">

                            <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 product   slide-1 ">
                                <img src="{{ asset('slides/slide1-1.jpg') }}" class=" img-product " id="img-p-1" >
                                <div>
                                    <p id="text-top-1" class="slide-text">I'll take one!</p>
                                    <p id="p-pic-1"class="slide-text">Häagen-Dazs <sup>®</sup> Neapolitan Strawberry Vanilla Milkshake</p>
                                </div>

                            </div>


                           <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 product   slide-1">
                                <img src="{{ asset('slides/slide1-2.png') }}" class=" img-product" id="img-p-2">
                                <div>
                                    <p id="text-top-2" class="slide-text">I want a taste!</p>
                                    <p id="p-pic-2"class="slide-text">Drumstick <sup>®</sup> Mini Drums <sup>™</sup> Carrot Cake </p>
                                </div>

                           </div>


                          <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 product  slide-1 ">
                                <img src="{{ asset('slides/slide1-3.png') }}" class=" img-product" id="img-p-3">
                                <div>
                                    <p id="text-top-3" class="slide-text">How refreshing!</p>
                                    <p id="p-pic-3" class="slide-text">Outshine <sup>®</sup> Mango Tajín <sup>®</sup> Frozen Mocktail</p>
                                </div>

                           </div>


                           <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 product  slide-1">
                                <img src="{{ asset('slides/slide1-4.jpg') }}" class=" img-product" id="img-p-4">

                                <div>
                                    <p id="text-top-4" class="slide-text">Mine!</p>
                                    <p id="p-pic-4" class="slide-text">Dreyer's <sup>™</sup> Teddy Bear Ice Cream Cones</p>
                                </div>

                          </div>

                       </div>
                     </div>


                      {{-- slide.2 --}}

                     <div class="carousel-item ">
                          <div class="row ">

                                <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 product  slide-2">
                                    <img src="{{ asset('slides/slide1-4.jpg') }}" class=" img-product " id="img-p-5">
                                     <div>
                                        <p id="text-top-5" class="slide-text">I want a taste!</p>
                                        <p id="p-pic-5" class="slide-text">Drumstick<sup>®</sup> Mini Drums <sup>™</sup> Carrot Cake</p>
                                     </div>
                               </div>

                                <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 product   slide-2">
                                    <img src="{{ asset('slides/slide2-1.jpg') }}" class=" img-product" id="img-p-6">
                                     <div>
                                        <p id="text-top-6" class="slide-text">Mmm, gimme!</p>
                                        <p id="p-pic-6" class="slide-text">Outshine <sup>®</sup> Mango Tajín <sup>®</sup> Frozen Mocktail</p>
                                     </div>
                               </div>

                               <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 product   slide-2">
                                    <img src="{{ asset('slides/slide2-2.jpg') }}" class=" img-product" id="img-p-7">
                                     <div>
                                        <p id="text-top-7" class="slide-text">Mine!</p>
                                        <p id="p-pic-7" class="slide-text">Dreyer's <sup>™</sup> Teddy Bear Ice Cream Cones</p>
                                     </div>
                                </div>

                                <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 product   slide-2">
                                    <img src="{{ asset('slides/slide2-3.jpg') }}" class=" img-product" id="img-p-8">
                                     <div>
                                        <p id="text-top-8" class="slide-text">Argh! Yum! Yes!</p>
                                          <p id="p-pic-8" class="slide-text">Häagen-Dazs <sup>®</sup> Vanilla Ice Cream Brownie Bars</p>
                                     </div>
                               </div>
                           </div>
                       </div>



                      {{-- slide.3 --}}
                       <div class="carousel-item  ">
                            <div class="row ">

                                <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 product   slide-3">
                                    <img src="{{ asset('slides/slide2-3.jpg') }}" class=" img-product" id="img-p-9">
                                <div>
                                    <p id="text-top-9" class="slide-text">Mmm, gimme!</p>
                                    <p id="p-pic-9" class="slide-text">Outshine <sup>®</sup> Mango Tajín <sup>®</sup> Frozen Mocktail</p>
                                </div>
                                </div>

                                <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 product   slide-3">
                                   <img src="{{ asset('slides/slide3-1.jpg') }}" class=" img-product" id="img-p-10">
                                <div>
                                    <p id="text-top-10" class="slide-text">Mine!</p>
                                    <p id="p-pic-10" class="slide-text">Dreyer's <sup>™</sup> Teddy Bear Ice Cream Cones</p>
                                </div>
                               </div>

                               <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 product    slide-3">
                                    <img src="{{ asset('slides/slide3-2.jpg') }}" class=" img-product" id="img-p-11">
                                    <div>
                                        <p id="text-top-11" class="slide-text">Argh! Yum! Yes!</p>
                                        <p id="p-pic-11" class="slide-text">Häagen-Dazs <sup>®</sup> Vanilla Ice Cream Brownie Bars</p>
                                    </div>
                                </div>

                               <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 product   slide-3">
                                    <img src="{{ asset('slides/slide3-3.jpg') }}" class=" img-product" id="img-p-12">
                                  <div>
                                    <p id="text-top-12" class="slide-text">Count me in!</p>
                                    <p id="p-pic-12" class="slide-text">Dreyer's <sup>™</sup> Salted Pretzel Magic Shell</p>
                                  </div>
                               </div>
                         </div>
                     </div>


                      <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                           <span class="visually-hidden">Previous</span>
                      </button>
                      <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                      </button>
                 </div>
             </div>
             <button type="button" class="btn-search">please Inter!</button>
           </div>
     <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6" id="div-1">
               <div>
                  <h5>The Rocky Road Collection</h5>
                  <h2>Rocky Road, super <br>smooth</h2>
                  <p >Check out the new Rocky Road Collection, a twist on the classic that makes every <br>bite super duper scrumptious!</p>
                  <button type="button" class="btn btn-success" >learn more!</button>
               </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6"  id="div-2" style="padding-top: 26px;width: 1;">
                   <img src="{{ asset('images/yammy.jpg') }}" >
           </div>
    </div>

    <div class="row">
        <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6" id="div-3" >
               <img src="{{ asset('images/girls.jpg') }}" >
        </div>

        <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6" id="div-4">
            <h5>Summer Season <br></h5>
            <h2 >Flavor fun, summer's begun</h2>
            <p >Your summer to remember starts with flavors you won't forget.</p>
            <button type="button" class="btn-splash" >take a splash!</button>

        </div>
    </div>



    <div class="row"  id="background-2">
            <h2 >Frequently Asked Ice Creams</h2><br><br><br><br>

            <div  class="col-sm-4 col-md-4 col-lg-4 col-xl-4   card-1" onmouseover="show_1_1()" onmouseout="show_1_2()">

                <div class="card-image  " >
                    <img src="{{ 'images/featurecard1.png' }}" alt=""  >
                </div>
                <div class="card-text">
                    <h6 >What's your flavor?</h6>
                    <br>
                    <br>
                    <p class="cmp-button__text cta-ff" id="card-txt-1">Find yours here</p>
                </div>

            </div>

            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4  card-2" onmouseover="show_2_1()" onmouseout="show_2_2()">
                <div class="card-image ">
                    <img src="{{ 'images/featurecard2.png' }}" alt="" >
                </div>
                <div class="card-text">
                    <h6>Dietary restrictions?</h6>
                    <h6>Need to know scoop sizes?</h6>
                    <br>
                   <p class="cmp-button__text cta-ff" id="card-txt-2">Visit the nutrition center</p>
                </div>
            </div>

           <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4 card-3"  onmouseover="show_3_1()" onmouseout="show_3_2()">
               <div class="card-image  ">
                   <img src="{{ 'images/featurecard3.png' }}" alt="" >
              </div>
              <div class="card-text">
                   <h6>History: A dessert </h6>
                   <h6>frozen through time</h6>
                   <br>
                   <p class="cmp-button__text cta-ff" id="card-txt-3">Learn the facts</p>
             </div>
          </div>
      </div>

      <div class="row " id="background3">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <img src="{{ asset('images/background3.jpeg') }}" alt="" class="contact-img">
            <h1>We all scream for ice cream!</h1>

            <img class ="product-ice"  id="product-ice-1" src="{{ asset('images/product-1.png') }}" alt="">
            <img class= "product-ice" id="product-ice-2"src="{{ asset('images/product-2.png') }}" alt="">

            <img class="product-ice" id="product-ice-3"src="{{ asset('images/product-3.png') }}" alt="">
            <img class="product-ice" id="product-ice-4"src="{{ asset('images/product-4.png') }}" alt="">


        </div>
      </div>





@endsection

