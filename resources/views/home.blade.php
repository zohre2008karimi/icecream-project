@extends('layouts.master')

@section('content')

 <div class="container home-container" >
  <div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 col-home">

        <img src="{{ asset('images/home/ice-home9.jpg') }}" id="home-img">

        <div>

                <h4 id="home-h4">Hi  {{ Auth::user()->name }} </h4>
                <p id="home-p">You Are Login </p>

                <a href="{{ route('logout') }}" class="logout link">
                    <label for="icofont-logout">LOGOUT</label>
               <i class="icofont-logout"></i>
            </a>

        </div>

    </div>
  </div>

 </div>
@endsection
